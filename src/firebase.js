// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
import { getAuth } from "firebase/auth";
import { getFirestore } from "@firebase/firestore"
import { getStorage } from "@firebase/storage"


const firebaseConfig = {
  apiKey: "AIzaSyAOjdRmrcvozwcG2__xG7D9ZNrDxtCeQjY",
  authDomain: "school-project-cb918.firebaseapp.com",
  projectId: "school-project-cb918",
  storageBucket: "school-project-cb918.appspot.com",
  messagingSenderId: "408292421328",
  appId: "1:408292421328:web:474c3a2bf15081d18b38d5"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
export const auth = getAuth(app)
export const firestore = getFirestore(app)
export const storage = getStorage(app)

