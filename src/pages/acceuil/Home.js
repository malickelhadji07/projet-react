import React from 'react';
import "./home.scss";
import Sidebar from '../../components/Sidebar';
import { Badge, Col, NavDropdown, Row } from 'react-bootstrap';
import { ArrowDown, ArrowRight, ArrowRightCircleFill, ArrowUp, BoxArrowInRight, Envelope, EnvelopeAtFill, Facebook, HouseDoorFill, Map, PeopleFill, PersonBadge, PersonCircle, PersonFill, PhoneFill, Twitter } from 'react-bootstrap-icons';
import { Link } from 'react-router-dom';
import { IoMan, IoWoman } from "react-icons/io5";
import BasicDemo from './BasicDemo'; // Import the chart component
import Img from "../../assets/img/propr.jpg"
import Img1 from "../../assets/img/pro.avif"
import Img3 from "../../assets/img/censeur.avif"


const Home = () => {
  const myCards = [
    { name: 'Eleves', counter: 420,  pourcentage: 83, icon: <PeopleFill size={30} color='#4e73df' />, icon2: <ArrowUp color='green'/> },
    { name: 'Professeurs', counter: 40,  pourcentage: 40, icon: <PersonCircle size={30} color='green' />, icon2: <ArrowDown color='red'/> },
    { name: 'Garçons', counter: 190,  pourcentage: 44,  icon: <IoMan size={30} color='blue' />,  icon2: <ArrowDown color='red'/>  },
    { name: 'Filles', counter: 230,  pourcentage: 66,  icon: <IoWoman size={30} color='pink' />, icon2: <ArrowUp color='green'/>  },
  ];

  const myInfos = [
    {photo:  Img, nom: "Elhadji Mlaick Diop", age: "20 ans", post: "Parrain" ,text: "Le proviseur dirige un collège ou un lycée. Il a la charge du fonctionnement général de l'établissement. Il supervise les services administratifs, gère les budgets, le personnel d'éducation et s'occupe de l'animation pédagogique auprès des professeurs. Il est leur supérieur hiérarchique direct."},
    {photo: Img1, nom: "Elhadji Mlaick Diop", age: "20 ans", post: "Proviseur" ,text: "Le proviseur dirige un collège ou un lycée. Il a la charge du fonctionnement général de l'établissement. Il supervise les services administratifs, gère les budgets, le personnel d'éducation et s'occupe de l'animation pédagogique auprès des professeurs. Il est leur supérieur hiérarchique direct."},
    {photo: Img3, nom: "Elhadji Mlaick Diop", age: "20 ans", post: "Censeur" ,text: "Le proviseur dirige un collège ou un lycée. Il a la charge du fonctionnement général de l'établissement. Il supervise les services administratifs, gère les budgets, le personnel d'éducation et s'occupe de l'animation pédagogique auprès des professeurs. Il est leur supérieur hiérarchique direct."}
  ]

  return (
    <div>
      {/* <Container fluid> */}
        <Row>
          <Col md={1} xs={1}>
            <Sidebar />
            <div className="img-user">
              <img className='logo-user' src={localStorage.getItem('profilePic')} alt="" />
            </div>
          </Col>
          <Col md={11} xs={12} className='bg-light'>
            <div className="myContent">
              <div className="header-user shadow d-flex justify-content-between align-items-center">
                <div className="logo-school fs-1 fw-bold text-white">
                  logo
                </div>
                <div className="me-md-4 text-white user-drop text-center user-name fw-bold">
                  <PersonFill size={30} color='black' /> 
                  <span title="Dropdown" id="basic-nav-dropdown">{localStorage.getItem('name')}</span>
                  <NavDropdown >
                    <Link className='ms-3 a1' to="/detail-user">Mon Compte</Link>
                    <span className='span1'><PersonBadge size={20}/></span>
                    <NavDropdown.Divider />
                    <Link className='ms-3 a2' to="/message">Message </Link>
                    <span className='span2 m-2'><Envelope/><Badge color='#4e73df' >4</Badge></span>
                    <NavDropdown.Divider />
                    <Link className='ms-3 a3' to="/">Deconnecter </Link>
                    <span className='span3'><BoxArrowInRight size={20}/></span>
                  </NavDropdown>
                </div>
              </div>
            </div>
            <div className="myContent1">
              <div className="myRow gap-2 my-3">
                {myCards.map((card, index) => (
                  <div className="col-md-3 col-11" key={index}>
                    <div className="left">
                      <span className='title'>{card.icon} {card.name}</span>
                      <span className='counter' style={{color: "#858796"}}>{card.counter}</span>
                      {/* <span  className='link'>
                        <Link to={`/detail-${index + 1}`}>plus de détails</Link>
                        <span className='arrow ms-1'><ArrowRightCircleFill/></span>
                      </span> */}
                    </div>
                    <div className={`right me-3 shadow ${card.pourcentage <= 50 ? 'text-danger' : 'text-success'}`}>
                      <div>{card.icon2}<span className={`fs-3 ${card.pourcentage <= 50 ? 'text-danger' : 'text-success'}`}>{card.pourcentage}%</span></div>
                    </div>
                  </div>
                ))}
              </div>
            </div>
            <div className="myContent2 d-md-flex align-items-center">
              <div className="left-text" style={{color: "#858796"}}>
                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Temporibus inventore minima enim mollitia ut deleniti dolor fuga, suscipit magnam, labore, atque ab repellat officiis! Similique, consequatur sit! Suscipit, omnis sed!
                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Temporibus inventore minima enim mollitia ut deleniti dolor fuga, suscipit magnam, labore, atque ab repellat officiis! Similique, consequatur sit! Suscipit, omnis sed!
                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Temporibus inventore minima enim mollitia ut deleniti dolor fuga, suscipit magnam, labore, atque ab repellat officiis! Similique, consequatur sit! Suscipit, omnis sed!
                <span  className='link'> <br /> <br />
                    <Link to='detail'>plus de détails</Link>
                    <span className='arrow ms-1'><ArrowRightCircleFill/></span>
                </span>
              </div>
              <div className="right-chart bg-light shadow">
                <BasicDemo />
              </div>
            </div>
            <div className="myContent3 d-md-flex align-items-center justify-content-evenly my-md-5">
                {myInfos.map((info, index) => (
                  <div className="myCards shadow" key={index}>
                      <div className="card-left d-flex justify-content-between">
                        <div>
                          <span className="photo ms-1 my-1">
                            <img className='photo' src={info.photo} alt="" />
                          </span>
                        </div>
                        <div className='d-flex flex-column me-1 my-1 border-bottom'>
                          <span className="name">{info.nom}</span>
                          <span className="name">{info.age}</span>
                          <span className='name'>{info.post}</span>
                        </div>
                      </div>
                      <div className="card-footer my-2">
                        {info.text}
                      </div>
                  </div>
                ))}
            </div>
            <div className="myContentFooter bg-light">
                <div className="d-flex flex-column logo-school fs-1 fw-bold" style={{color: "#858796"}}>
                  <span>Logo</span>
                  <span className='fw-light'><HouseDoorFill/> Lycée Alboury Ndiaye</span>
                  <span className='fw-light'><Map/>BM 7321 - Linguere, Senegal</span>
                  <span className='fw-light'><PhoneFill/>(+221) 77 123 45 67</span>
                  <hr />
                </div>
                <div className="theLinks d-flex justify-content-center justify-content-around">
                <div className="menu">
                    <h6 className='head-menu'>A-Propos</h6>
                    <div className="h6"></div>
                    <ul>
                      <li className='d-md-flex gap-2'>
                        <span><ArrowRight/></span>
                        <span><Link to='/home'>Qui somme nous ?</Link></span>
                      </li>
                      <li className='d-md-flex gap-2'>
                        <span><ArrowRight/></span>
                        <span><Link to='/home'>Creation du Lycée</Link></span>
                      </li>
                      <li className='d-md-flex gap-2'>
                        <span><ArrowRight/></span>
                        <span><Link to='/home'>Le Parrain</Link></span>
                      </li>
                    </ul>
                  </div>
                  <div className="menu">
                    <h6 className='head-menu'>Etudier ici ?</h6>
                    <div className="h6"></div>
                    <ul>
                      <li className='d-md-flex gap-2'>
                        <span><ArrowRight/></span>
                        <span><Link to='/home'>Inscription</Link></span>
                      </li>
                      <li className='d-md-flex gap-2'>
                        <span><ArrowRight/></span>
                        <span><Link to='/home'>Admission</Link></span>
                      </li>
                      <li className='d-md-flex gap-2'>
                        <span><ArrowRight/></span>
                        <span><Link to='/home'>Comment nous rejoindre ?</Link></span>
                      </li>
                    </ul>
                  </div>
                  <div className="menu">
                    <h6 className='head-menu'>Contact</h6>
                    <div className="h6"></div>
                  <div className="mesLinks d-flex flex-column">
                    <Link><span><Facebook/> Facebook</span></Link>
                    <Link><span><Twitter/> Twitter</span></Link>
                    <Link><span><EnvelopeAtFill/> Email</span></Link>
                  </div>
                  </div>
                  
                </div>
                <p className='text-center my-5'>copyright © 2024 | tous les droits sont reserves.</p>
            </div>
          </Col>
        </Row>
      {/* </Container> */}
    </div>
  );
};

export default Home;
