import React, { useState } from 'react';
import './learn.scss'
import Sidebar from '../../components/Sidebar';
import { Col, Row } from 'react-bootstrap';
import { Link } from 'react-router-dom';

const Learn = () => {

    const mesCours = [
        {matiere: "Fr", question: "tu veux apprendre des cours français ?", respons: "Veuillez cliquer sur un lien pour apprendre"},
        {matiere: "HG", question: "tu veux apprendre des cours Histoires-Géos ?", respons: "Veuillez cliquer sur un lien pour apprendre"},
        {matiere: "Ang", question: "tu veux apprendre des cours  Anglais ?", respons: "Veuillez cliquer sur un lien pour apprendre"}
    ]

    const [searchInput, setSearchInput] = useState('');
    
    const filterMesCours = mesCours.filter((cour) =>
    cour.matiere.toLowerCase().includes(searchInput.toLowerCase())
    );

    const handleFilter = (e) => {
        const inputValue = e.target.value;
        setSearchInput(inputValue);
    };


    return (
        // <Container fluid className='learn'>
            <Row className='learn bg-body-secondary'>
                <Col md={1} xs={12}>
                    <Sidebar/>
                </Col>
                <Col md={11}xs={12}>
                    <div className="francais my-2">
                        <h3 style={{letterSpacing: "2px"}} className='fw-bold text-dark'>Les Cours</h3>
                    </div>
                    <div className="search my-2">
                        <input
                            type="text"
                            className='form-control w-100'
                            placeholder='Rechercher une leçon'
                            value={searchInput}
                            onChange={handleFilter}
                        />
                    </div>
                  
                    <div className="d-md-flex justify-content-center justify-content-around  align-items-center" >
                        {
                            filterMesCours.map((cour, k) => (
                                <div className="card-cour mb-2 my-5" key={k}>
                                    <div className={`card-learn p-3 m-auto ${k === 1 ? 'card-learn-1' : k === 2 ? 'card-learn-2' : ''}`}>
                                        <div className="d-flex gap-2">
                                            <div className="head-card p-3 fw-bold text-light">
                                                {cour.matiere}
                                            </div>
                                            <div className="head-card-text">
                                                <p className='text-light fw-bold'>{cour.question}</p>
                                            </div>
                                        </div>
                                        <div className="myborder"></div>
                                       <footer>
                                            <div className="footer-learn my-4">
                                                    <div className="pdf-text">
                                                        <p className='text-light'>{cour.respons}</p>
                                                    </div>
                                                <div className="footer-btn d-flex gap-3 my-3">
                                                    <div className="pdf p-1">
                                                        <Link className='text-light' to="/pdf">Documents</Link>
                                                    </div>
                                                    <div className="pdf p-1">
                                                        <Link className='text-light' to="/pdf">Vidéos</Link>
                                                    </div>
                                                </div>
                                            </div>
                                       </footer>
                                    </div>
                                </div> 
                        ))}          
                    </div>
                   
                </Col>
            </Row>
        // </Container>
    );
};

export default Learn;