import React, { useRef, useState } from 'react';
import { Link } from 'react-router-dom';
import "./sign.scss";
import { Facebook, Google } from 'react-bootstrap-icons';
import { auth, firestore } from '../../firebase';
import { createUserWithEmailAndPassword } from 'firebase/auth';
import { addDoc, collection } from "@firebase/firestore";

const Sign = () => {
    const [values, setValues] = useState({
        prenom: "",
        nom: "",
        email: "",
        number: "",
        password: "",
        confirmPassword: "",
    });

    const handleChange = (e) => {
        setValues({
            ...values,
            [e.target.name]: e.target.value
        });
    }

    const messageRef = useRef();
    const messagesCollection = collection(firestore, "données");
    const [errorMessage, setErrorMessage] = useState(false);
    const [Validmodal, setValidModal] = useState(false);

    const handleSumbit = async (e) => {
        e.preventDefault();

        if (
            values.prenom === '' ||
            values.nom === '' ||
            values.number === '' ||
            values.email === '' ||
            values.password === '' ||
            values.confirmPassword === ''
        ) {
            alert('Vous devez remplir tous les champs');
            return;
        } else if (values.password !== values.confirmPassword) {
            alert('Les mots de passe ne correspondent pas');

            // Réinitialiser seulement les champs password et confirmPassword
            setValues((prevValues) => ({
                ...prevValues,
                password: '',
                confirmPassword: '',
            }));
            return;

        } else if (values.password.length < 8) {
            setErrorMessage(<div className="alert alert-danger text-center">Le mot de passe doit avoir au moins 8 caractères</div>);
            return;
        } else if (values.password === values.confirmPassword) {
            // Create user with email and password
            createUserWithEmailAndPassword(auth, values.email, values.password)
                .then((userCredential) => {
                    const user = userCredential.user;
                    console.log('User created:', user);
                    setValidModal(true);
                })
                .catch((error) => {
                    console.error('Error creating user:', error);
                    setValidModal(true);
                });

            const data = {
                name: values.prenom,
                nom: values.nom,
                email: values.email,
                number: values.number,
                password: values.password,
                comfirmPassword: values.confirmPassword,
            };

            try {
                await addDoc(messagesCollection, data);
                alert('Document successfully written!');

                setValues({
                    nom: '',
                    prenom: '',
                    email: '',
                    number: '',
                    password: '',
                    confirmPassword: '',
                });

            } catch (error) {
                console.error('Error writing document: ', error);
                alert('Error writing document. Check the console for details.');
            }
        }
    };

    return (
        <div className="body-login">
            {Validmodal && <div className='sign-modal justify-content-center d-flex flex-column align-items-center'>
                <div className="text-center">Votre compte à été créé avec succès ! <br />
                    <Link to='/home' className='shadow text-secondary p-2' style={{ background: "#fff" }}>Je me connecte</Link>
                </div>
                <div className="reducing-line"></div>
            </div>}
            <div className='sign'>
                <div className="box">
                    <form onSubmit={handleSumbit} ref={messageRef}>
                        <h2>S'inscrire</h2>
                        <div className="d-md-flex gap-3">
                            <div className="inputbox">
                                <input
                                    id='prenom'
                                    type="text"
                                    name='prenom'
                                    value={values.prenom}
                                    onChange={handleChange}
                                    required="required" />
                                <span>Prénom</span>
                                <i></i>
                            </div>
                            <div className="inputbox">
                                <input
                                    id='nom'
                                    type="text"
                                    name='nom'
                                    value={values.nom}
                                    onChange={handleChange}
                                    required="required" />
                                <span>Nom</span>
                                <i></i>
                            </div>
                        </div>
                        <div className="d-md-flex gap-3">
                            <div className="inputbox">
                                <input
                                    id='email'
                                    type="email"
                                    name='email'
                                    value={values.email}
                                    onChange={handleChange}
                                    required="required" />
                                <span>Email</span>
                                <i></i>
                            </div>
                            <div className="inputbox">
                                <input
                                    id='number'
                                    type="tel"
                                    name='number'
                                    value={values.number}
                                    onChange={handleChange}
                                    required="required" />
                                <span>Telephone</span>
                                <i></i>
                            </div>
                        </div>
                        <div className="d-md-flex gap-3">
                            <div className="inputbox">
                                <input
                                    id='password'
                                    type="password"
                                    name='password'
                                    value={values.password}
                                    onChange={handleChange}
                                    required="required" />
                                <span>Mot de passe</span>
                                <i></i>
                            </div>
                            <div className="inputbox">
                                <input
                                    value={values.confirmPassword}
                                    type='password'
                                    id='confirmPassword'
                                    name='confirmPassword'
                                    onChange={handleChange}
                                    required="required"
                                />
                                <span>Confirmer le mot de passe</span>
                                <i></i>
                            </div>
                        </div>
                        <div className="links">
                            <Link to="/">Se connecter</Link>
                        </div>
                        <div className="btn">
                            <button className='btn btn-light my-3 w-75' style={{ color: '#8f8f8f' }}>Inscrire</button>
                        </div>
                        {errorMessage}
                    </form>
                </div>
                <div className="loginIcon d-flex gap-4 my-2">
                    <button className="icon icon1 d-flex justify-content-between align-items-center">
                        <span className='ms-md-5 text-light'>connecter avec</span>
                        <><Facebook size={30} color='#fff' /></>
                    </button>
                    <button className="icon icon2 d-flex justify-content-between align-items-center">
                        <span className='ms-md-5 text-light'>connecter avec</span>
                        <><Google size={30} color='#fff' /></>
                    </button>
                </div>
            </div>
        </div>
    );
};

export default Sign;
