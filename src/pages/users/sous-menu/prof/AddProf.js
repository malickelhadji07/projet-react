import React, { useState, useEffect, useRef } from 'react';
import axios from 'axios';
import { firestore, storage } from '../../../../firebase';
import { ref, uploadBytes, getDownloadURL } from '@firebase/storage';
import { addDoc, collection } from "@firebase/firestore";
import { Link, useNavigate } from 'react-router-dom';
import "./prof.scss";
import { Check2Circle, PersonCircle } from 'react-bootstrap-icons';

const AddProf = () => {
  const [validModal, setValidModal] = useState(false);
  const navigate = useNavigate();

  useEffect(() => {
    const cardChild = document.querySelector('.card-child');
    const handleAnimationEnd = () => {
      const timeoutId = setTimeout(() => {
        navigate('/user');
      }, 4000);
  
      return () => {
        clearTimeout(timeoutId);
      };
    };
  
    if (cardChild) {
      cardChild.addEventListener('animationend', handleAnimationEnd);
    }
  
    return () => {
      if (cardChild) {
        cardChild.removeEventListener('animationend', handleAnimationEnd);
      }
    };
  }, [navigate]);
  
  const [values, setValues] = useState({
    fullName: '',
    profile: null,
    email: '',
    tel: '',
    status: '',
  });

  const [imageUrl, setImageUrl] = useState('');

  const handleChange = (e) => {
    const { name, value, type, files } = e.target;
    const newValue = type === 'file' ? files[0] : value;

    setValues({
      ...values,
      [name]: newValue,
    });
  };

  const message2Ref = useRef();
  const messagesCollection = collection(firestore, "professeurs");

  const handleAjoute = async (e) => {
    e.preventDefault();

    const { fullName, email, tel, status } = values;

    if (!fullName || !email || !tel || !status) {
      console.error('Please fill in all fields');
      return;
    }

    try {
      const storageRef = ref(storage, `images/${values.image.name}`);
      await uploadBytes(storageRef, values.image);

      const imageUrl = await getDownloadURL(storageRef);
      setImageUrl(imageUrl);
      setValidModal(true); 

      setTimeout(() => {
        navigate('/user');
      }, 4000);

      const response = await axios.post('http://localhost:3008/professeurs', {
        fullName: fullName,
        profile: imageUrl,
        email: email,
        tel: tel,
        status: status,
      });

      console.log('professeur added successfully:', response.data);

      setImageUrl('');
    } catch (error) {
      console.error('Error adding addProf:', error.message);
    }

    const data = {
      fullName: values.fullName,
      email: values.email,
      tel: values.tel,
      status: values.status,
    };

      try {
      await addDoc(messagesCollection, data);
      alert('Document successfully written!');
      setValues({
          fullName: '',
          email: '',
          tel: '',
          status: ''
      });

      } catch (error) {
          console.error('Error writing document: ', error);
          alert('Error writing document. Check the console for details.');
      }
  };

  return (
    <div>
      <h2 className='text-center shadow border-3'>Ajouter un Professeur <PersonCircle/></h2>
      <form className='my-5 form-lesson' onSubmit={handleAjoute} ref={message2Ref}>
        <div className="input-1 d-flex gap-3 justify-content-center p-2">
          <input
            type="text"
            name="fullName"
            value={values.fullName}
            onChange={handleChange}
            className='form-control shadow'
            placeholder="Prenom et Nom"
          />
          <input
            type="email"
            name="email"
            value={values.email}
            onChange={handleChange}
            className='form-control shadow'
            placeholder="Email"
          />
        </div>
        <div className="input-1 d-flex gap-3 justify-content-center p-2">
          <input
            type="tel"
            name="tel"
            value={values.tel}
            onChange={handleChange}
            className='form-control shadow'
            placeholder="telephone"
          />
          <input
            type="text"
            name="status"
            value={values.status}
            onChange={handleChange}
            className='form-control shadow'
            placeholder="Professeur de....."
          />
          {imageUrl && <img src={imageUrl} alt="Utilisateur" />}
        </div>
        <div className="input-file d-flex gap-md-5 justify-content-center">
          <input
            className='form-control shadow'
            type="file"
            name="image"
            accept="image/*"
            onChange={handleChange}
          />
          <div className="submit-prof">
            <button type="submit" className="btn btn-success">Ajouter</button>
          </div>
        </div>
      </form>

      <div className="card-valide">
        {validModal && 
          <div className='card-child justify-content-center d-flex flex-column align-items-center slide-in'>
            <div className="text-center">Le Professeur à été ajouté avec succès <Check2Circle color='lime' size={30}/> <br />
              <Link to='/user' className='shadow text-white p-2' style={{ background: "#4e73df" }}>Ok</Link>
            </div>
            <div className="reducing-line"></div>
          </div>
        }
      </div>
    </div>
  );
};

export default AddProf;
