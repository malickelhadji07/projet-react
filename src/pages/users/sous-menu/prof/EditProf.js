import React, { useState, useEffect } from 'react';
import "./prof.scss";
import { Col, Container, Row } from 'react-bootstrap';
import axios from 'axios';
import { Link, useNavigate, useParams } from 'react-router-dom';
import { ref, uploadBytes, getDownloadURL } from '@firebase/storage';
import { storage } from '../../../../firebase';


const Edit = () => {

    const [imageUrl, setImageUrl] = useState('');
    const handleChange = (e) => {
      
      const { name, value, type, files } = e.target;
      const newValue = type === 'file' ? files[0] : value;
  
      setValues({
        ...values,
        [name]: newValue,
      });
    };

    const [values, setValues] = useState({
        fullName: "",
        status: "",
        email: "",
        tel: "",
        profile: null
    });
    

    const { id } = useParams();
    const navigate = useNavigate();

    useEffect(() => {
        // Fetch professor data by ID and populate the form
        const fetchProfessorData = async () => {
            try {
                const response = await axios.get(`http://localhost:3008/professeurs/${id}`);
                const professorData = response.data;

                // Populate the form fields with professor data
                setValues({
                    fullName: professorData.fullName,
                    status: professorData.status,
                    email: professorData.email,
                    tel: professorData.tel,
                });
            } catch (error) {
                console.error("Error fetching professor data:", error);
            }
        };

        fetchProfessorData();
    }, [id]);

    const onChange = (e) => {
        setValues({ ...values, [e.target.name]: e.target.value });
    };

    const updateProfessor = async () => {
        
        try {
            // Upload de l'image vers Firebase Storage
            const storageRef = ref(storage, `images/${values.image.name}`);
            await uploadBytes(storageRef, values.image);
      
            // Récupération de l'URL d'image
            const imageUrl = await getDownloadURL(storageRef);
      
            // Enregistrement de l'URL d'image dans le state
            setImageUrl(imageUrl);
      
            // Afficher une alerte
            alert('L\'image a été modifiée avec succès!');
            // Send the updated professor data to the server
            await axios.put(`http://localhost:3008/professeurs/${id}`, {
                fullName: values.fullName,
                profile: imageUrl,
                email: values.email,
                tel: values.tel,
                status: values.status,
            })

            // Handle success
            console.log("Professeur updated successfully");

            // Navigate to the '/profs' route
            navigate('/user');
        } catch (error) {
            console.error("Error updating professeur:", error);
        }

    };

    return (
        <div>
            <Container>
                <Row>
                    <Col md={6} className='text-center m-auto my-5 fw-bold bg-light border shadow'>
                        <h5 className='my-3 shadow p-2 bg-light'>Modifier un Professeur</h5>
                        <div className="form my-4">
                            <div className='d-flex gap-4'>
                                <input
                                    type="text"
                                    name='fullName'
                                    placeholder='Prénom et Nom'
                                    className='form-control border shadow'
                                    value={values.fullName}
                                    onChange={onChange}
                                />
                                <input
                                    type="text"
                                    name='status'
                                    placeholder='Professeur de...'
                                    className='form-control border shadow'
                                    value={values.status}
                                    onChange={onChange}
                                />
                            </div> <br />

                            <div className='d-flex gap-4'>
                                <input
                                    type="email"
                                    name='email'
                                    placeholder='E-mail'
                                    className='form-control border shadow'
                                    value={values.email}
                                    onChange={onChange}
                                />
                                <input
                                    type="tel"
                                    name='tel'
                                    placeholder='Téléphone'
                                    className='form-control border shadow'
                                    value={values.tel}
                                    onChange={onChange}
                                />
                            </div> <br />

                            {imageUrl && <img src={imageUrl} alt="Utilisateur" />}

                            <div className="input-file d-flex justify-content-center me-md-4">
                                <input
                                    className='form-control shadow'
                                    type="file"
                                    name="image"
                                    accept="image/*"
                                    onChange={handleChange}
                                />
                                <div className="submit-prof ms-md-5 me-2 ">
                                    <Link to={`/detailprof/${id}`} className='btn btn-primary border shadow'>retour</Link>
                                    <button className='btn btn-success border shadow ms-2' onClick={updateProfessor}>Modifier</button>
                                </div>
                            </div>
        
                        </div>
                    </Col>
                </Row>
            </Container>
        </div>
    );
};

export default Edit;
