import axios from 'axios';
import React, { useState, useEffect } from 'react';
import { Col, Container, Row } from 'react-bootstrap';
import { ArchiveFill, EyeFill, PencilFill, PlusCircleFill, Trash } from 'react-bootstrap-icons';
import { Link, useNavigate } from 'react-router-dom';
import './prof.scss';

const TableProfs = () => {
  const [professeurs, setProfesseurs] = useState([]);
  const [searchInput, setSearchInput] = useState('');
  const [deletingProfId, setDeletingProfId] = useState(null);
  const [adminModalVisible, setAdminModalVisible] = useState(false);
  const [myButton, setMyButton] = useState('...')
  const [adminPassword, setAdminPassword] = useState(true);
  const [noValid, setNoValid] = useState(false)
  const navigate = useNavigate();

  useEffect(() => {
    const cardChild = document.querySelector('.card-child');
    const handleAnimationEnd = () => {
        // setDeletingProfId(null)
    };

    if (cardChild) {
      cardChild.addEventListener('animationend', handleAnimationEnd);
    }

    return () => {
      if (cardChild) {
        cardChild.removeEventListener('animationend', handleAnimationEnd);
      }
    };
  }, [navigate]);

  useEffect(() => {
    fetchProfesseurs();
  }, []);

//   affiche mes donnes depuis db.json
  const fetchProfesseurs = () => {
    axios.get('http://localhost:3008/professeurs')
      .then((response) => setProfesseurs(response.data))
      .catch((error) => console.error("Error fetching professeurs:", error));
  };

  //supprimer un prof avec l'affiche du modal et temps
  const handleDelete = (profId) => {
    setDeletingProfId(profId);

    setTimeout(() => {
      if (deletingProfId === profId) {
        confirmDelete(profId);
      }
      confirmDelete(profId);
      setDeletingProfId(null)
    }, 10000);
  };


//   si je confirme il supprime
  const confirmDelete = (profId) => {
    axios
      .delete(`http://localhost:3008/professeurs/${profId}`)
      .then((response) => {
        setProfesseurs(professeurs.filter((prof) => prof.id !== profId));
        setDeletingProfId(null);
      })
      .catch((error) => console.error("Error deleting professor:", error));
  };

  //si j'annuler le supprime
  const cancelDelete = () => {
    setDeletingProfId(null);
  };

  const filterProfesseurs = professeurs.filter((prof) =>
    prof.fullName.toLowerCase().includes(searchInput.toLowerCase())
  );

  const handleFilter = (e) => {
    const inputValue = e.target.value;
    setSearchInput(inputValue);
  };

  const HandleAjoute = () => {
      // alert(`acces nos autorise`)
      navigate('/addprof')
  }

  const [actions, setActions] = useState(false)
  const handlePasswordSubmit = () => {
    setAdminModalVisible(true);
    if (adminPassword === 'admin') {
      setAdminModalVisible(false)
      setAdminPassword('');
      setAdminModalVisible(false);
      setMyButton(false)
      setActions(true);
    } else if (!adminPassword){
      setNoValid(<div className='alert alert-danger text-center text-danger'>Mot de passe incorrect *_*</div>)
    }
    return;
  };

  return (
    <div className='tableProf'>
      <Container fluid>
        <Row>
          <Col md={12} className='prof-header my-2'>
            <h3>Listes des Professeurs</h3>
            <div className="header-table d-flex align-items-center justify-content-between my-3">
              <div className="search">
                <input
                  type="text"
                  className='form-control'
                  placeholder='Recherche un professeur...'
                  value={searchInput}
                  onChange={handleFilter}

                />
              </div>
              <div className="prof text-end my-2">
                <button className='profAdd' onClick={HandleAjoute}>Ajouter <PlusCircleFill color='#4e73df'/></button>
              </div>
            </div>
            <div className="listes text-center">
              <table className='myTable table shadow'>
                <thead className='thead'>
                  <tr>
                    <th>ID</th>
                    <th>Profile</th>
                    <th>Nom complet</th>
                    <th>Email</th>
                    <th>Téléphone</th>
                    <th>Statut</th>
                    <th>Actions</th>
                  </tr>
                </thead>
                <tbody>
                  {filterProfesseurs.map((prof, index) => (
                    <tr key={index}>
                      <td className='fw-bold'>{prof.id}</td>
                      <td className='profile'>
                        <img src={prof.profile} alt='img' />
                      </td>
                      <td>{prof.fullName}</td>
                      <td>{prof.email}</td>
                      <td>{prof.tel}</td>
                      <td>{prof.status}</td>
                      <td className='m-2'>
                       {myButton && <button 
                       onClick={handlePasswordSubmit} 
                       className='btn btn-primary text-white fw-bold'>
                        ...
                      </button>
                        }
                        {
                          actions && <div className="mesActions">
                          <Link to={`/detailprof/${prof.id}`} className='btn-icon m-1'><EyeFill color='#4e73df' size={18}/></Link>
                          <Link to={`/edit/${prof.id}`} className='btn-icon m-1'><PencilFill color='yellow' size={18}/></Link>
                          <Link to={`/archive/${prof.id}`} className='btn-icon m-1'><ArchiveFill color='wheat' size={18}/></Link>
                          <button className='btn-icon m-1' onClick={() => handleDelete(prof.id)}>
                            <Trash color='red'/>
                          </button>
                          </div>
                        }
                      </td>
                    </tr>
                  ))}
                </tbody>
              </table>
            </div>
            <div className="card-valide">
              {deletingProfId && (
                <div className='card-child justify-content-center d-flex flex-column align-items-center slide-in'>
                  <div className="text-center">
                    Le Professeur sera supprimé dans 10 secondes
                    <br />
                    <button
                      onClick={() => confirmDelete(deletingProfId)}
                      className="shadow text-white p-2"
                      style={{ background: "red" }}
                    >
                      Supprimer
                    </button>
                    <button
                      onClick={cancelDelete}
                      className="shadow text-white p-2 ms-2"
                      style={{ background: "lime" }}
                    >
                      Annuler
                    </button>
                  </div>
                  <div className="reducing-line"></div>
                </div>
              )}
            </div>

            {adminModalVisible && (
                    <div className="modal-password">
                        <div className="myModal">
                            <label htmlFor="admin" className='text-white fw-bold my-1'>Seule L'administrateur peut l'utiliser</label>
                            <input
                                type="password"
                                placeholder='Mot de passe admin'
                                className='form-control'
                                value={adminPassword}
                                onChange={(e) => setAdminPassword(e.target.value)}
                            />
                            <div className="d-flex gap-2">
                            <button className='w-100 btn btn-info text-white my-2' onClick={handlePasswordSubmit} >Enter</button>
                            </div>

                            {noValid}
                                         
                        </div>
                    </div>
              )}
          </Col>
        </Row>
      </Container>
    </div>
  );
};

export default TableProfs;
