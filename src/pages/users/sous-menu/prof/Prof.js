import React, { /*useState*/ } from 'react';
import { /*Carousel*/ Col, Container, Row } from 'react-bootstrap';
// import { EnvelopeFill, Facebook, PersonBoundingBox, PersonCircle, Whatsapp } from 'react-bootstrap-icons';
// import { useMediaQuery } from 'react-responsive';
import TableProfs from './TableProfs';


const Prof = () => {
    // Données des professeurs
    // const professeurs = [
    //     {
    //         nom: 'Mme',
    //         prenom: 'Diop',
    //         age: 34, 
    //         matiere: 'Professeur Philosophie',
    //         presentation: `Lorem ipsum dolor sit amet consectetur adipisicing elit 
    //         Lorem ipsum dolor sit amet consectetur adipisicing elit 
    //         Lorem ipsum dolor sit amet consectetur adipisicing eli`,
    //         reseauxSociaux: [
    //             { icone: <Facebook size={20} color='blue' />, couleur: 'blue' },
    //             { icone: <Whatsapp size={20} color='lime' />, couleur: 'lime' },
    //             { icone: <EnvelopeFill size={20} color='red' />, couleur: 'red' },
    //         ],
    //     },
    //     {
    //         nom: 'Mr',
    //         prenom: 'Simon',
    //         age: 41,
    //         matiere: 'Professeur Français',
    //         presentation: `Lorem ipsum dolor sit amet consectetur adipisicing elit 
    //         Lorem ipsum dolor sit amet consectetur adipisicing elit 
    //         Lorem ipsum dolor sit amet consectetur adipisicing eli`,
    //         reseauxSociaux: [
    //             { icone: <Facebook size={20} color='blue' />, couleur: 'blue' },
    //             { icone: <Whatsapp size={20} color='lime' />, couleur: 'lime' },
    //             { icone: <EnvelopeFill size={20} color='red' />, couleur: 'red' },
    //         ],
    //     },
    //     {
    //         nom: 'Mr',
    //         prenom: 'Kahn',
    //         age: 44,
    //         matiere: 'Professeur EPS',
    //         presentation: `Lorem ipsum dolor sit amet consectetur adipisicing elit 
    //         Lorem ipsum dolor sit amet consectetur adipisicing elit 
    //         Lorem ipsum dolor sit amet consectetur adipisicing eli`,
    //         reseauxSociaux: [
    //             { icone: <Facebook size={20} color='blue' />, couleur: 'blue' },
    //             { icone: <Whatsapp size={20} color='lime' />, couleur: 'lime' },
    //             { icone: <EnvelopeFill size={20} color='red' />, couleur: 'red' },
    //         ],
    //     },
    //     {
    //         nom: 'Mme',
    //         prenom: 'Seck',
    //         age: 29,
    //         matiere: 'Professeur Histoire-Geo',
    //         presentation: `Lorem ipsum dolor sit amet consectetur adipisicing elit 
    //         Lorem ipsum dolor sit amet consectetur adipisicing elit 
    //         Lorem ipsum dolor sit amet consectetur adipisicing eli`,
    //         reseauxSociaux: [
    //             { icone: <Facebook size={20} color='blue' />, couleur: 'blue' },
    //             { icone: <Whatsapp size={20} color='lime' />, couleur: 'lime' },
    //             { icone: <EnvelopeFill size={20} color='red' />, couleur: 'red' },
    //         ],
    //     },
    //     {
    //         nom: 'Mr',
    //         prenom: 'Hane',
    //         age: 30,
    //         matiere: 'Professeur Français',
    //         presentation: `Lorem ipsum dolor sit amet consectetur adipisicing elit 
    //         Lorem ipsum dolor sit amet consectetur adipisicing elit 
    //         Lorem ipsum dolor sit amet consectetur adipisicing eli`,
    //         reseauxSociaux: [
    //             { icone: <Facebook size={20} color='blue' />, couleur: 'blue' },
    //             { icone: <Whatsapp size={20} color='lime' />, couleur: 'lime' },
    //             { icone: <EnvelopeFill size={20} color='red' />, couleur: 'red' },
    //         ],
    //     },
    //     {
    //         nom: 'Sir',
    //         prenom: 'Gueye',
    //         age: 26,
    //         matiere: 'Professeur Anglais',
    //         presentation: `Lorem ipsum dolor sit amet consectetur adipisicing elit 
    //         Lorem ipsum dolor sit amet consectetur adipisicing elit 
    //         Lorem ipsum dolor sit amet consectetur adipisicing eli`,
    //         reseauxSociaux: [
    //             { icone: <Facebook size={20} color='blue' />, couleur: 'blue' },
    //             { icone: <Whatsapp size={20} color='lime' />, couleur: 'lime' },
    //             { icone: <EnvelopeFill size={20} color='red' />, couleur: 'red' },
    //         ],
    //     },
    //     {
    //         nom: 'Mme',
    //         prenom: 'leye',
    //         age: 36,
    //         matiere: 'Professeur Espagnol',
    //         presentation: `Lorem ipsum dolor sit amet consectetur adipisicing elit 
    //         Lorem ipsum dolor sit amet consectetur adipisicing elit 
    //         Lorem ipsum dolor sit amet consectetur adipisicing eli`,
    //         reseauxSociaux: [
    //             { icone: <Facebook size={20} color='blue' />, couleur: 'blue' },
    //             { icone: <Whatsapp size={20} color='lime' />, couleur: 'lime' },
    //             { icone: <EnvelopeFill size={20} color='red' />, couleur: 'red' },
    //         ],
    //     },
    //     {
    //         nom: 'Sir',
    //         prenom: 'Diop',
    //         age: 22,
    //         matiere: 'Anglais',
    //         presentation: `Lorem ipsum dolor sit amet consectetur adipisicing elit 
    //         Lorem ipsum dolor sit amet consectetur adipisicing elit 
    //         Lorem ipsum dolor sit amet consectetur adipisicing eli`,
    //         reseauxSociaux: [
    //             { icone: <Facebook size={20} color='blue' />, couleur: 'blue' },
    //             { icone: <Whatsapp size={20} color='lime' />, couleur: 'lime' },
    //             { icone: <EnvelopeFill size={20} color='red' />, couleur: 'red' },
    //         ],
    //     },
    //     {
    //       nom: 'Mme',
    //       prenom: 'Ndiaye',
    //       age: 30,
    //       matiere: 'Professeur Histoire-Geo',
    //       presentation: `Lorem ipsum dolor sit amet consectetur adipisicing elit 
    //       Lorem ipsum dolor sit amet consectetur adipisicing elit 
    //       Lorem ipsum dolor sit amet consectetur adipisicing eli`,
    //       reseauxSociaux: [
    //           { icone: <Facebook size={20} color='blue' />, couleur: 'blue' },
    //           { icone: <Whatsapp size={20} color='lime' />, couleur: 'lime' },
    //           { icone: <EnvelopeFill size={20} color='red' />, couleur: 'red' },
    //       ],
    //     },
    //     {
    //       nom: 'Segnior',
    //       prenom: 'Sene',
    //       age: 39,
    //       matiere: 'Professeur Italien',
    //       presentation: `Lorem ipsum dolor sit amet consectetur adipisicing elit 
    //       Lorem ipsum dolor sit amet consectetur adipisicing elit 
    //       Lorem ipsum dolor sit amet consectetur adipisicing eli`,
    //       reseauxSociaux: [
    //           { icone: <Facebook size={20} color='blue' />, couleur: 'blue' },
    //           { icone: <Whatsapp size={20} color='lime' />, couleur: 'lime' },
    //           { icone: <EnvelopeFill size={20} color='red' />, couleur: 'red' },
    //       ],
    //     },
    //     {
    //       nom: 'Segnorita',
    //       prenom: 'Angela',
    //       age: 28,
    //       matiere: 'Professeur Italien',
    //       presentation: `Lorem ipsum dolor sit amet consectetur adipisicing elit 
    //       Lorem ipsum dolor sit amet consectetur adipisicing elit 
    //       Lorem ipsum dolor sit amet consectetur adipisicing eli`,
    //       reseauxSociaux: [
    //           { icone: <Facebook size={20} color='blue' />, couleur: 'blue' },
    //           { icone: <Whatsapp size={20} color='lime' />, couleur: 'lime' },
    //           { icone: <EnvelopeFill size={20} color='red' />, couleur: 'red' },
    //       ],
    //     },
    //     {
    //       nom: 'Mr',
    //       prenom: 'Tounkara',
    //       age: 42,
    //       matiere: 'Professeur Philosophie',
    //       presentation: `Lorem ipsum dolor sit amet consectetur adipisicing elit 
    //       Lorem ipsum dolor sit amet consectetur adipisicing elit 
    //       Lorem ipsum dolor sit amet consectetur adipisicing eli`,
    //       reseauxSociaux: [
    //           { icone: <Facebook size={20} color='blue' />, couleur: 'blue' },
    //           { icone: <Whatsapp size={20} color='lime' />, couleur: 'lime' },
    //           { icone: <EnvelopeFill size={20} color='red' />, couleur: 'red' },
    //       ],
    //     },
        
    // ];

    // const Professeur = ({ prof }) => (
    //     <div className="the-prof">
    //       <div className="info1-prof d-flex">
    //         <div className="photo-prof">
    //           <PersonBoundingBox size={50}/>
    //         </div>
    //         <div className="text1-prof d-flex flex-column">
    //           <span className="name ms-2">{prof.nom}</span>
    //           <span className="name ms-2">{prof.prenom}</span>
    //           <span className="name ms-2">{prof.age} ans</span>
    //         </div>
    //       </div>
    //       <div className="matiere-prof text-center d-flex flex-column">
    //         <span className="matiere">{prof.matiere}</span>
    //         <span className='mini-text'>{prof.presentation}</span>
    //       </div>
    //       <span className="info-prof">
    //         <span className="info2-prof">
    //           <h6 className="text-center my-3 text-white">Présentation</h6>
    //           <span className="mini-text d-flex my-5">{prof.presentation}</span>
    //           <span className="icon-prof d-flex align-items-center justify-content-center gap-3">
    //             {prof.reseauxSociaux.map((reseau, index) => (
    //               <div key={index} className="span-icon" style={{ color: reseau.couleur }}>
    //                 {reseau.icone}
    //               </div>
    //             ))}
    //           </span>
    //         </span>
    //       </span>
    //     </div>
    //   );
    
    //   const isLaptop = useMediaQuery({ minWidth: 992 });
    //   const professorsPerSlide = isLaptop ? 4 : 1;
    
    //   const [index, setIndex] = useState(0);
    
    //   const handleSelect = (selectedIndex, e) => {
    //     setIndex(selectedIndex);
    //   };
    
      return (
        <div>
          <Container>
            {/* <Row>
              <Col md={12}>
                <div className="prof-header">
                  <h2>Les Professeurs <PersonCircle/></h2>
                  <p>liste de tous les professeurs qui enseignent dans cet établissement</p>
                  <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Commodi, similique! Non id porro dolor tempora earum suscipit eveniet eius eligendi. Tenetur nam ut tempora ex expedita numquam blanditiis dolorum similique!</p>
                </div>
    
                <Carousel
                  className="liste-profs d-md-flex justify-content-around"
                  activeIndex={index}
                  onSelect={handleSelect}
                  interval={10000}  // Disable automatic sliding
                  touch 
                >
                  {professeurs.map((prof, index) => (
                    index % professorsPerSlide === 0 && (
                      <Carousel.Item key={index}>
                        <Row>
                          {professeurs
                            .slice(index, index + professorsPerSlide)
                            .map((prof, innerIndex) => (
                              <Col key={innerIndex} md={12 / professorsPerSlide}>
                                <Professeur prof={prof} />
                              </Col>
                            ))}
                        </Row>
                      </Carousel.Item>
                    )
                  ))}
                </Carousel>
    
              </Col>
            </Row> */}
            <Row>
              <Col md={12}>
                <TableProfs/>
              </Col>
            </Row>
          </Container>
        </div>
      );
    };
    
    export default Prof;