import React, { useState, useEffect } from 'react';
import { Link, useParams } from 'react-router-dom';

const DetailProf = () => {
  const { id } = useParams();
  const [prof, setProf] = useState(null);

  useEffect(() => {
    const fetchUser = async () => {
      try {
        const response = await fetch(`http://localhost:3008/professeurs/${id}`);
        if (!response.ok) {
          throw new Error("Unexpected Server Response");
        }

        const data = await response.json();
        setProf(data);
        
      } catch (error) {
        console.error("Error:", error);
      }
    };

    fetchUser();
  }, [id]);

  if (!prof) {
    return <p>Loading...</p>;
  }

  return (
    <div className='border shadow m-auto w-50 my-5'>
      <h2 className='text-center shadow'>Professeur</h2>
      <div className="p-5 d-flex justify-content-between">
        <div className="info">
          <p><span className='fw-bold ms-3'>ID </span>: {prof.id}</p>
          <p><span className='fw-bold ms-3'>Prénom et Nom</span>: {prof.fullName}</p>
          <p><span className='fw-bold ms-3'>Email</span>: {prof.email}</p>
          <p><span className='fw-bold ms-3'>Téléphone</span>: {prof.tel}</p>
          <p><span className='fw-bold ms-3'>Status</span>: {prof.status}</p>
          <Link to="/user" className='btn btn-primary ms-3'>Back</Link>
          <Link to={`/edit/${prof.id}`} className='btn btn-warning ms-3'>Modifier</Link>
        </div>
        <div className="my-4 shadow border border-4 h-25">
            <img src={prof.profile} alt="profile du professeur" 
              style={{width: '180px', height: '160px', scale: "0.9"}}
              className='my-3'
            />
        </div>
      </div>
    </div>
  );
};

export default DetailProf;
