
import React, { useState, useEffect } from 'react';
import { Chart } from 'primereact/chart';

export default function LineDemo() {
    const [chartData, setChartData] = useState({});
    const [chartOptions, setChartOptions] = useState({});

    useEffect(() => {
        const documentStyle = getComputedStyle(document.documentElement);
        const textColor = documentStyle.getPropertyValue('--text-color');
        const textColorSecondary = documentStyle.getPropertyValue('--text-color-secondary');
        const surfaceBorder = documentStyle.getPropertyValue('--surface-border');
        const data = {
            labels: ['1993', '2001', '2006', '2011', '2016', '2021', '2023'],
            datasets: [
                {
                    label: 'Augmentation des classes depuis 1993',
                    data: [5, 13, 15, 17, 19, 20],
                    fill: false,
                    borderColor: '#4e73df',
                    tension: 0.4
                }
            ]
        };
        const options = {
            maintainAspectRatio: false,
            aspectRatio: 0.6,
            plugins: {
                legend: {
                    labels: {
                        color: textColor
                    }
                }
            },
            scales: {
                x: {
                    ticks: {
                        color: textColorSecondary
                    },
                    grid: {
                        color: surfaceBorder
                    }
                },
                y: {
                    ticks: {
                        color: textColorSecondary
                    },
                    grid: {
                        color: surfaceBorder
                    }
                }
            }
        };

        setChartData(data);
        setChartOptions(options);
    }, []);

    
    const chartStyle = {
        width: 'auto',
        height: '300px'
    };

    return (
        <div>
            <Chart type="line" data={chartData} options={chartOptions} style={chartStyle}/>
        </div>
    )
}
        