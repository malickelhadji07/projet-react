import React, { useState } from 'react';
import { Col, Container, Row } from 'react-bootstrap';
import "./eleve.scss"
import { Link } from 'react-router-dom';
import LineDemo from './BasicDemo2';

const Eleve = () => {
    const myClasses = [
        { name: "2nd_A" }, { name: "2nd_B" }, { name: "2nd_C" }, { name: "2nd_D" },
        { name: "2nd-S A" }, { name: "2nd-S B" }, { name: "1er-L A" }, { name: "1er L B" },
        { name: "1er-L'A" }, { name: "1er-L'B" }, { name: "1er-L'C" }, { name: "1er-S A" },
        { name: "1er-S A" }, { name: "T-L2 A" }, { name: "T-L2 B" }, { name: "T-L'A" },
        { name: "T-L'B" }, { name: "T-L'C" }, { name: "T-S A" }, { name: "T-S B" },
    ];

    const [searchInput, setSearchInput] = useState('');
    const [adminModalVisible, setAdminModalVisible] = useState(true);
    const [adminPassword, setAdminPassword] = useState('');
    const [authorized, setAuthorized] = useState(false);
    const [adminNoValid, setAdminNoValid] = useState(false)

    const filterMyClasses = myClasses.filter((classe) =>
        classe.name.toLowerCase().includes(searchInput.toLowerCase())
    );

    const handleFilter = (e) => {
        const inputValue = e.target.value;
        setSearchInput(inputValue);
    };

    const handleEnter = () => {
        // Check admin password and set authorization status
        if (adminPassword === 'admin') {
            setAdminModalVisible(false);
            setAuthorized(false);
        } else {
            setAdminPassword('')
            setAdminNoValid(<div className="alert alert-danger">Mot de passe incorrect *_*</div>)
        }

    };

    return (
        <div className='eleve'>
            <Container fluid>
                {!authorized && (
                    <Row>
                    <div className="title-classe">
                        <h2 className='fw-bold' style={{ color: "#a1a1a1" }}>Liste des classes</h2>
                    </div>
                    <div className="search">
                        <input
                            type="text"
                            className='form-control'
                            placeholder='Rechercher une classe A'
                            value={searchInput}
                            onChange={handleFilter}
                        />
                    </div>
                    {filterMyClasses.map((classe, k) => (
                        <Col md={3} xs={12} key={k} className='p-2'>
                            <Link to={`/classe${classe.name}`}>
                                <div className= 'shadow d-flex justify-content-center align-items-center'>
                                    <div className="name-class">
                                        <div className="left fw-bold">
                                            {classe.name}
                                        </div>
                                        <div className="right"></div>
                                    </div>
                                </div>
                            </Link>
                        </Col>
                    ))}
                </Row>
                )}
                {adminModalVisible && (
                    <div className="modal-password">
                        <div className="myModal">
                            <label htmlFor="admin" className='text-white fw-bold my-1'>Admin</label>
                            <input
                                type="password"
                                placeholder='Mot de passe admin'
                                className='form-control'
                                value={adminPassword}
                                onChange={(e) => setAdminPassword(e.target.value)}
                            />
                            <div className="d-flex gap-2">
                            <button className='w-100 btn btn-light text-dark my-2' onClick={handleEnter}>Enter</button>
                            </div>
                           
                                {adminNoValid}
                            
                        </div>
                    </div>
                )}
            </Container>
            <div className="chart eleve">
                <LineDemo/>
            </div>
        </div>
    );
};

export default Eleve;
