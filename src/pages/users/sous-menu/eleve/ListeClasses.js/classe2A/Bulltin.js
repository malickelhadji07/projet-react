import axios from 'axios';
import React, { useEffect, useState } from 'react';
import { PrinterFill } from 'react-bootstrap-icons';

const Bulltin = () => {
    const [nameEleve, setNameEleve] = useState([]);
    useEffect(() => {
        fetchEleves();
    }, []);

    //   affiche mes données depuis db.json
    const fetchEleves = () => {
        axios.get('http://localhost:3008/eleves')
            .then((response) => setNameEleve(response.data))
            .catch((error) => console.error("Error fetching eleves:", error));
    };

    const handlePrint = () => {
        window.print();
    };

    return (
        <div>
            <div className="imprimer my-1">
                <button className='btn btn-info text-light' onClick={handlePrint}>
                    <span><PrinterFill/></span>
                    <span className='ms-2'>Imprimer</span>
                </button>
            </div>
            <div className="note-bulletin">
                <div className="d-flex justify-content-between">
                    <div className="head-note">
                        <div className="haut fs-5">Dakar</div>
                        <div className="bas fs-5">Ruffixe</div>
                    </div>
                    <div className="head-note">
                        <div className="text-end haut fs-5">2023/2024</div>
                        <div className="bas fs-5">1er semester</div>
                    </div>
                </div>
                <div className="text-uppercase border-top border-bottom border-dark text-center my-2 fs-3">
                    Bulletin de Notes
                </div>
                <div className="lesNotes d-flex justify-content-between">
                    <div className="left d-flex flex-column">
                        <span>Prénom et Nom: <span className="fw-bold">{nameEleve.length > 0 && nameEleve[0].fullName}</span></span>
                        <span>Date de Naissance: <span className="fw-bold">23/02/2008 à Dakar</span></span>
                        <span>Nombre d'élèves: <span className="fw-bold">46</span></span>
                    </div>
                    <div className="right d-flex flex-column">
                        <span>Classe: <span className="fw-bold">2nd A</span></span>
                        <span>Classe redoublée: <span className="fw-bold">1</span></span>
                    </div>
                </div>
                <div className="table-responsive">
                    <table className="table table-bordered my-3">
                        <thead>
                            <tr>
                                <th>Matière</th>
                                <th>Devoir</th>
                                <th>Compo</th>
                                <th>Coef</th>
                                <th>Moy/20</th>
                                <th>Rang</th>
                                <th>Appréciations</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>Français</td>
                                <td>12</td>
                                <td>11.5</td>
                                <th>3</th>
                                <th>11,75</th>
                                <th>19</th>
                                <th>Assez-bien</th>
                            </tr>
                            <tr>
                                <td>Histoire-Géo</td>
                                <td>12</td>
                                <td>11.5</td>
                                <th>3</th>
                                <th>11,75</th>
                                <th>19</th>
                                <th>Assez-bien</th>
                            </tr>
                            <tr>
                                <td>Éducation Civique</td>
                                <td>12</td>
                                <td>11.5</td>
                                <th>3</th>
                                <th>11,75</th>
                                <th>19</th>
                                <th>Assez-bien</th>
                            </tr>
                            <tr>
                                <td>Mathématiques</td>
                                <td>12</td>
                                <td>11.5</td>
                                <th>3</th>
                                <th>11,75</th>
                                <th>19</th>
                                <th>Assez-bien</th>
                            </tr>
                            <tr>
                                <td>Physique-Chimie</td>
                                <td>12</td>
                                <td>11.5</td>
                                <th>3</th>
                                <th>11,75</th>
                                <th>19</th>
                                <th>Assez-bien</th>
                            </tr>
                            <tr>
                                <td>Espagnol</td>
                                <td>12</td>
                                <td>11.5</td>
                                <th>3</th>
                                <th>11,75</th>
                                <th>19</th>
                                <th>Assez-bien</th>
                            </tr>
                            <tr>
                                <td>Anglais</td>
                                <td>12</td>
                                <td>11.5</td>
                                <th>2</th>
                                <th>11,75</th>
                                <th>19</th>
                                <th>Assez-bien</th>
                            </tr>
                            <tr>
                                <td>EPS</td>
                                <td>12</td>
                                <td>11.5</td>
                                <th>1</th>
                                <th>11,75</th>
                                <th>19</th>
                                <th>Assez-bien</th>
                            </tr> <br />
                            <tr className='fw-bold'>
                                <td>TOTAL</td>
                                <td></td>
                                <td></td>
                                <th>21</th>
                                <th>106</th>
                               
                            </tr>
                            <tr  className='fw-bold'>
                                <td>MOYENNE</td>
                                <td>11.75 /20</td>
                                <td>Rang</td>
                                <td>19</td>
                                <td>Retard</td>
                                <td>0</td>
                                <td>Absence: 05</td>
                                
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div className="d-flex justify-content-between">
                    <table className='table table-bordered my-3'>
                        <thead>
                            <tr>
                                <td>Félicitations</td>
                                <td>X</td>
                            </tr>
                        </thead>
                        <tbody>
                           <tr>
                                <td>Encouragement</td>
                                <td></td>
                           </tr>
                           <tr>
                                <td>Autorité à redoublé</td>
                                <td></td>
                           </tr>
                           <tr>
                                <td>Tableau d'honneur</td>
                                <td>X</td>
                           </tr>
                           <tr>
                                <td>Avertissement</td>
                                <td></td>
                           </tr> 
                           <tr>
                                <td>Blame</td>
                                <td></td>
                           </tr>
                        </tbody>
                    </table>  
                    <table className='table table-bordered my-3'>
                        <thead>
                            <tr>
                                <td>Félicitations</td>
                                <td>X</td>
                            </tr>
                        </thead>
                        <tbody>
                           <tr>
                                <td>Encouragement</td>
                                <td></td>
                           </tr>
                           <tr>
                                <td>Autorité à redoublé</td>
                                <td></td>
                           </tr>
                           <tr>
                                <td>Tableau d'honneur</td>
                                <td>X</td>
                           </tr>
                           <tr>
                                <td>Avertissement</td>
                                <td></td>
                           </tr> 
                           <tr>
                                <td>Blame</td>
                                <td></td>
                           </tr>
                        </tbody>
                    </table>    
                    </div>
            </div>
        </div>
    );
};

export default Bulltin;
