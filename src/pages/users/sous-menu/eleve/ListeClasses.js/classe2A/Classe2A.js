import axios from 'axios';
import React, { useEffect, useState } from 'react';
import { Container, Row } from 'react-bootstrap';
import { ArrowLeftCircleFill, FileTextFill, LayoutTextSidebarReverse, PencilFill, PlusCircleFill, Trash} from 'react-bootstrap-icons';
import { FaUser } from "react-icons/fa";
import { Link, useNavigate } from 'react-router-dom';
import './classe2A.scss'

const Classe2A = () => {
    const [searchInput, setSearchInput] = useState('');
    const [eleves, setEleves] = useState([])
    const navigate = useNavigate()

    const filterMyEleves = eleves.filter((classe) =>
    classe.fullName.toLowerCase().includes(searchInput.toLowerCase())
    );

    const handleFilter = (e) => {
        const inputValue = e.target.value;
        setSearchInput(inputValue);
    };

    
    useEffect(() => {
        fetchEleves();
    }, []);

    //   affiche mes donnes depuis db.json
    const fetchEleves = () => {
        axios.get('http://localhost:3008/eleves')
        .then((response) => setEleves(response.data))
        .catch((error) => console.error("Error fetching eleves:", error));
    };

    //   si je confirme il supprime
    const handleDelete = (eleveId) => {
        axios
        .delete(`http://localhost:3008/eleves/${eleveId}`)
        .then((response) => {
            setEleves(eleves.filter((eleve) => eleve.id !== eleveId));
        })
        .catch((error) => console.error("Error deleting professor:", error));
    };

    const HandleAjoute = () => {
        // alert(`acces nos autorise`)
        navigate('/addeleve')
    }

    return (
        <div>
            <Container>
                    <div className="mytitle my-1">
                        <span><Link to='/user'><ArrowLeftCircleFill size={25}/></Link></span>
                        <h3 className='fw-bold' style={{color: '#a1a1a1'}}>Listes des élèves de la 2nd A</h3>
                        <div className="search">
                        <input
                            type="text"
                            className='form-control'
                            placeholder='Rechercher un eleve'
                            value={searchInput}
                            onChange={handleFilter}
                        />
                        </div>
                        <div className="prof text-end my-2">
                            <button className='profAdd' onClick={HandleAjoute}>Ajouter <PlusCircleFill color='#4e73df'/></button>
                        </div>
                    </div>
                <Row>


                    <div className="listes text-center">
                        <table className='myTable table shadow'>
                            <thead className='thead'>
                            <tr>
                                <th>ID</th>
                                <th>Nom complet</th>
                                <th>User</th>
                                <th>Téléphone</th>
                                <th>Age</th>
                                <th>Sexe</th>
                                <th>Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            {filterMyEleves.map((eleve, index) => (
                                <tr key={index}>
                                <td className='fw-bold'>{eleve.id}</td>
                                <td>{eleve.fullName}</td>
                                <td><FaUser/></td>
                                <td>{eleve.tel}</td>
                                <th>{eleve.age} ans</th>
                                <th>{eleve.sexe}</th>
                                <td>
                                    <div className="d-flex justify-content-center">
                                    <div className="icon-span">
                                        <Link to={`/editeleve/${eleve.id}`} className='btn-icon'><PencilFill color='yellow' size={18}/></Link>
                                        <span className='edit'>Modifier</span>
                                    </div>
                                    <div className="icon-span">
                                        <Link to={`/archive/${eleve.id}`} className='btn-icon'><FileTextFill color='blue' size={18}/></Link>
                                        <span className='edit'>Note</span>
                                    </div>
                                    <div className="icon-span">
                                        <Link to={`/bulletin/${eleve.id}`} className='btn-icon'><LayoutTextSidebarReverse color='wheat' size={18}/></Link>
                                        <span className='edit'>Buttin</span>
                                    </div>
                                    <div className="icon-span">
                                        <button className='btn-icon' onClick={() => handleDelete(eleve.id)}>
                                            <Trash color='red'/>
                                        </button> 
                                    </div>
                                  
                                    </div>
                                </td>
                                </tr>
                            ))}
                            </tbody>
                        </table>
                    </div>
                </Row>
            </Container>
        </div>
    );
};

export default Classe2A;