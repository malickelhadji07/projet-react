import React, { useState, useEffect } from 'react';
// import "./Classe2A.scss";
import { Col, Container, Row } from 'react-bootstrap';
import axios from 'axios';
import { Link, useNavigate, useParams } from 'react-router-dom';


const EditEleve = () => {

    const [values, setValues] = useState({
        fullName: "",
        tel: "",
        sexe: "",
        age: ""
    });
    

    const { id } = useParams();
    const navigate = useNavigate();

    useEffect(() => {
        // Fetch professor data by ID and populate the form
        const fetchProfessorData = async () => {
            try {
                const response = await axios.get(`http://localhost:3008/eleves/${id}`);
                const eleveData = response.data;

                // Populate the form fields with professor data
                setValues({
                    fullName: eleveData.fullName,
                    tel: eleveData.tel,
                    sexe: eleveData.sexe,
                    age: eleveData.age,
                });
            } catch (error) {
                console.error("Error fetching professor data:", error);
            }
        };

        fetchProfessorData();
    }, [id]);

    const onChange = (e) => {
        setValues({ ...values, [e.target.name]: e.target.value });
    };

    const updateEleve = async () => {
        
        try {
            await axios.put(`http://localhost:3008/eleves/${id}`, {
                fullName: values.fullName,
                tel: values.tel,
                sexe: values.sexe,
                age: values.age,
            })

            // Handle success
            console.log("eleve updated successfully");

            // Navigate to the '/profs' route
            navigate('/classe2nd_A');
        } catch (error) {
            console.error("Error updating professeur:", error);
        }

    };

    return (
        <div>
            <Container>
                <Row>
                    <Col md={6} className='text-center m-auto my-5 fw-bold bg-light border shadow'>
                        <h5 className='my-3 shadow p-2 bg-light'>Modifier l'élève</h5>
                        <div className="form my-4">
                            <div className='d-flex gap-4'>
                                <input
                                    type="text"
                                    name='fullName'
                                    placeholder='Prénom et Nom'
                                    className='form-control border w-100 shadow'
                                    value={values.fullName}
                                    onChange={onChange}
                                />
                            </div> <br />

                            <div className='d-flex gap-4'>
                                <input
                                    type="tel"
                                    name='tel'
                                    placeholder='Téléphone'
                                    className='form-control border w-100 shadow'
                                    value={values.tel}
                                    onChange={onChange}
                                />
                            </div> <br />

                            <div className='d-flex gap-4'>
                                <select className='form-select border w-50' value={values.sexe}>
                                    <option selected>sex</option>
                                    <option value="Masculin">Masculin</option>
                                    <option value="Feminin">Feminin</option>
                                </select>
                                <input
                                    type="age"
                                    name='age'
                                    placeholder='Votre age'
                                    className='form-control border shadow'
                                    value={values.age}
                                    onChange={onChange}
                                />
                            </div> <br />

                               
                                <div className="submit-prof text-end">
                                    <Link to="/classe2nd_A" className='btn btn-primary border shadow'>retour</Link>
                                    <button className='btn btn-success border shadow ms-2' onClick={updateEleve}>Modifier</button>
                                </div>
        
                        </div>
                    </Col>
                </Row>
            </Container>
        </div>
    );
};

export default EditEleve;
