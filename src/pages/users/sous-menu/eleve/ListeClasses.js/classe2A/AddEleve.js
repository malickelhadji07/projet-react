import React, { useState, useEffect } from 'react';
import axios from 'axios';
import { Link, useNavigate } from 'react-router-dom';
import { Check2Circle } from 'react-bootstrap-icons';
import { FaUser } from "react-icons/fa";
import '../../../prof/prof.scss';

const AddEleve = () => {
  const [validModal, setValidModal] = useState(false);
  const [repmlir, setRemplir] = useState(false);
  const navigate = useNavigate();

  useEffect(() => {
    const cardChild = document.querySelector('.card-child');
    const handleAnimationEnd = () => {
      const timeoutId = setTimeout(() => {
        navigate('/classe2nd_A');
      }, 4000);

      return () => {
        clearTimeout(timeoutId);
      };
    };

    if (cardChild) {
      cardChild.addEventListener('animationend', handleAnimationEnd);
    }

    return () => {
      if (cardChild) {
        cardChild.removeEventListener('animationend', handleAnimationEnd);
      }
    };
  }, [navigate]);

  const [values, setValues] = useState({
    fullName: '',
    tel: '',
    sexe: '',
    age: '',
  });

  const handleChange = (e) => {
    setValues({ ...values, [e.target.name]: e.target.value });
  };

  const validateForm = () => {
    if (values.fullName === '' || values.tel === '' || values.sexe === '' || values.age === '') {
      setRemplir(<div className="alert alert-warning">Vous devez tout remplir !</div>);
      return false;
    } else {
        setValidModal(true);
        setTimeout(() => {
          navigate('/classe2nd_A');
        }, 4000);
    }
    return true;
  };

  const handleAjoute = async (e) => {
    e.preventDefault();

    if (!validateForm()) {
      return;
    }

    try {
      const response = await axios.post('http://localhost:3008/eleves', {
        fullName: values.fullName,
        age: values.age,
        tel: values.tel,
        sexe: values.sexe,
      });

      // Handle successful response
      console.log('élève added successfully:', response.data);

      // Reset the form values
      setValues({
        fullName: '',
        tel: '',
        age: '',
        sexe: '',
      });
    } catch (error) {
      console.error('Error adding élève:', error.message);
    }
  };

  return (
    <div>
      <h2 className="text-center shadow border-3">Ajouter un élève <FaUser /></h2>
      <form className="my-5 form-lesson" onSubmit={handleAjoute}>
        <div className="input-1 d-flex gap-3 justify-content-center p-2">
          <input
            type="text"
            name="fullName"
            value={values.fullName}
            onChange={handleChange}
            className="form-control shadow"
            placeholder="Prenom et Nom"
          />

          <input
            type="tel"
            name="tel"
            value={values.tel}
            onChange={handleChange}
            className="form-control shadow"
            placeholder="telephone"
          />
        </div>
        <div className="input-1 d-flex gap-3 justify-content-center p-2">
          <select
            name="sexe"
            value={values.sexe}
            onChange={handleChange}
            className="form-select border shadow"
            style={{ width: '300px' }}
            >
            <option value="">Sélectionnez le sexe</option>
            <option value="Masculin">Masculin</option>
            <option value="Feminin">Feminin</option>
            </select>
          <input
            type="text"
            name="age"
            value={values.age}
            onChange={handleChange}
            className="form-control shadow"
            placeholder="Votre age"
          />
        </div>
        <div className="input-file d-flex gap-md-5 justify-content-center">
          <div className="submit-prof">
            <button type="submit" className="btn btn-success">Ajouter</button>
          </div>
        </div>
        <div className="alert w-25 text-center m-auto">
          {repmlir}
        </div>
      </form>

      <div className="card-valide">
        {validModal && (
          <div className="card-child justify-content-center d-flex flex-column align-items-center slide-in">
            <div className="text-center">Le Professeur à été ajouté avec succès <Check2Circle color="lime" size={30} /> <br />
              <Link to="/classe2nd_A" className="shadow text-white p-2" style={{ background: "#4e73df" }}>Ok</Link>
            </div>
            <div className="reducing-line"></div>
          </div>
        )}
      </div>
    </div>
  );
};

export default AddEleve;
