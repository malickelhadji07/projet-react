import React from 'react';
import Sidebar from '../../components/Sidebar';
import { Col, Row, Tab, Tabs } from 'react-bootstrap';
import "./user.scss"
import Eleve from './sous-menu/eleve/Eleve';
import Prof from './sous-menu/prof/Prof';


const User = () => {
    return (
        <div>
            {/* <Container fluid> */}
                <Row>
                    <Col md={1} xs={1}>
                        <Sidebar/>
                    </Col>
                    <Col md={11} xs={12}>
                        <div className="user-content bg-light">
                            <div className="user-header">
                                <Tabs
                                defaultActiveKey="home"
                                id="uncontrolled-tab-example"
                                className="mb-3"
                                >
                                    <Tab eventKey="home" title="Professeurs">
                                        <Prof/>
                                    </Tab>
                                    <Tab eventKey="profile" title="Classes">
                                        <Eleve/>
                                    </Tab>
                                </Tabs>
                            </div>
                        </div>
                    </Col>
                </Row>
            {/* </Container> */}
        </div>
    );
};

export default User;