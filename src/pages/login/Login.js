import React, { useState } from 'react';
import "./login.scss";
import { Link, useNavigate } from 'react-router-dom';
import { auth } from '../../firebase';
import { signInWithEmailAndPassword } from 'firebase/auth';
import { EmojiAngryFill } from 'react-bootstrap-icons';


const Login = () => {
    const [values, setValues] = useState({
        email: "",
        password: ""
    });

    const [loading, setLoading] = useState(false);
    const [errorMessage, setErrorMessage] = useState(null);

    const handleChange = (e) => {
        setValues({
            ...values,
            [e.target.name]: e.target.value
        });
    }

    const navigate = useNavigate();

    const handleSubmit = async (e) => {
        e.preventDefault();

        setLoading(true);

        try {
            const userCredential = await signInWithEmailAndPassword(auth, values.email, values.password);
            const user = userCredential.user;
            console.log(user);

            setValues({
                email: '',
                password: '',
            });

            // Additional logic when the user is successfully authenticated
            if (user) {
                setTimeout(() => {
                    setLoading(false);
                    navigate('/home');
                    // Add any other actions you want to perform here
                }, 1000); // Adjust the duration as needed
            }
        } catch (error) {
            console.log(error);
            setErrorMessage(<div className="alert alert-danger text-center">Ce compte n'existe pas <EmojiAngryFill/> </div>);
            setLoading(false);
        }
    };

    return (
        <div className="body-login">
            <div className="login">
                <div className="box">
                    <form onSubmit={handleSubmit}>
                        <h2>Se connecter</h2>
                        <div className="inputbox">
                            <input
                                id='email'
                                type="email"
                                name='email'
                                value={values.email}
                                onChange={handleChange}
                                required="required"
                                disabled={loading} // Disable input during loading
                            />
                            <span>Email</span>
                            <i></i>
                        </div>
                        <div className="inputbox">
                            <input
                                id='password'
                                type="password"
                                name='password'
                                value={values.password}
                                onChange={handleChange}
                                required="required"
                                disabled={loading} // Disable input during loading
                            />
                            <span>Mot de passe</span>
                            <i></i>
                        </div>
                        <div className="links">
                            <Link to="/">Mot de passe oublié ?</Link>
                            <Link to="/sign">S'inscrire</Link>
                        </div>
                        <button type='submit' className='btn btn-light my-3' style={{ color: '#8f8f8f' }}>
                            Connecter
                        </button>
                        {errorMessage}
                        {loading && <div className="loader m-auto my-3"></div>}
                    </form>
                </div>
            </div>
        </div>
    );
};

export default Login;
