import React, { useEffect } from 'react';
import { Route, Routes, Navigate, useNavigate } from 'react-router-dom';

const PrivateRoute = ({ isAuth, component: Component, ...rest }) => {
  const navigate = useNavigate()
  useEffect(() => {
    if (isAuth) {
      navigate('/home');
    }
  }, [isAuth]);

  return (
    <Routes>
      <Route {...rest} element={isAuth ? <Navigate to="/" /> : <Component />} />
    </Routes>
  );
};

export default PrivateRoute;
