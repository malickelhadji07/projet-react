import React, { useState } from 'react';
import { BoxArrowInRight, Envelope, HouseFill, ListNested, PeopleFill, PersonBadge, PersonFill } from 'react-bootstrap-icons';
import { GiRead } from "react-icons/gi";
import { IoBookSharp } from "react-icons/io5";
import { RiSchoolFill } from "react-icons/ri";
import { IoIosContact } from "react-icons/io";

import './sidebar.scss';
import { Link } from 'react-router-dom';
import { Badge, NavDropdown } from 'react-bootstrap';

const Sidebar = () => {
  const [isSidebarActive, setSidebarActive] = useState(false);

  const toggleSidebar = () => {
    setSidebarActive(!isSidebarActive);
  };

  return (
    <div className='mysiderbar fixed-top'>
      <header>
          <div className="toggle" onClick={toggleSidebar}>
            <ListNested size={20}/>
          </div>
          <h3>DashBoard</h3>
          <Link to="/home">
          <div className="text-white user-drop text-center user-name fw-bold">
                  <PersonFill size={30} color='black' /> 
                  <span title="Dropdown" id="basic-nav-dropdown">{localStorage.getItem('name')}</span>
                  <NavDropdown >
                    <Link className='ms-3 a1' to="/detail-user">Mon Compte</Link>
                    <span className='span1'><PersonBadge size={20}/></span>
                    <NavDropdown.Divider />
                    <Link className='ms-3 a2' to="/message">Message </Link>
                    <span className='span2 m-2'><Envelope/><Badge color='#4e73df' >4</Badge></span>
                    <NavDropdown.Divider />
                    <Link className='ms-3 a3' to="/">Deconnecter </Link>
                    <span className='span3'><BoxArrowInRight size={20}/></span>
                  </NavDropdown>
                </div>
          </Link>
      </header>

      <nav className={`sidebar ${isSidebarActive ? 'active' : ''}`}>
        <ul>
          <li>
            <Link to="#" className='toggle' onClick={toggleSidebar}>
              <span className='icon'><ListNested size={20}/></span>
              <span className='title'>Menu</span>
            </Link>
          </li>
          <li>
            <Link to="/home">
              <span className='icon'><HouseFill size={20}/></span>
              <span className='title'>Home</span>
            </Link>
          </li>
          <li>
            <Link to="/user">
              <span className='icon'><PeopleFill size={20}/></span>
              <span className='title'>Users</span>
            </Link>
          </li>
          <li>
            <Link to="/learn">
              <span className='icon'><GiRead size={20}/></span>
              <span className='title'>Bibliothéque</span>
            </Link>
          </li>
          <li>
            <Link to="/learn">
              <span className='icon'><IoBookSharp size={20}/></span>
              <span className='title'>Apprendre</span>
            </Link>
          </li>
          <li>
            <Link to="/learn">
              <span className='icon'><RiSchoolFill size={20}/></span>
              <span className='title'>Etablissement</span>
            </Link>
          </li>
          <li>
            <Link to="/learn">
              <span className='icon'><IoIosContact size={20}/></span>
              <span className='title'>Nous rejoindre</span>
            </Link>
          </li>
        </ul>
      </nav>
    </div>
  );
};

export default Sidebar;
