import './App.css';
// import Sidebar from './components/Sidebar';
import { BrowserRouter, Route, Routes } from 'react-router-dom';
import Login from './pages/login/Login';
import Sign from './pages/sign/Sign';
import Home from './pages/acceuil/Home';
import User from './pages/users/User';
import Learn from './pages/Learn.js/Learn';
import AddProf from './pages/users/sous-menu/prof/AddProf';
import EditProf from './pages/users/sous-menu/prof/EditProf';
import DetailProf from './pages/users/sous-menu/prof/DetailProf';
import Archive from './pages/users/sous-menu/prof/Archive';
import Classe from './pages/users/sous-menu/eleve/Classe';
import Classe2A from './pages/users/sous-menu/eleve/ListeClasses.js/classe2A/Classe2A';
import EditEleve from './pages/users/sous-menu/eleve/ListeClasses.js/classe2A/EditEleve';
import AddEleve from './pages/users/sous-menu/eleve/ListeClasses.js/classe2A/AddEleve';
import Bulltin from './pages/users/sous-menu/eleve/ListeClasses.js/classe2A/Bulltin';

function App() {
  return (
    <div className="App">
       <BrowserRouter>
            <Routes>
                  <Route path='/' element={<Login/>}/>
                  <Route path='/sign' element={<Sign/>}/>
                  <Route index element={<Home/>} path='/home'/>
                  <Route path='/user' element={<User/>}/>
                  <Route path='/learn' element={<Learn/>}/>
                  <Route path='/addprof' element={<AddProf/>}/>
                  <Route path='/edit/:id' element={<EditProf/>}/>
                  <Route path='/detailprof/:id' element={<DetailProf/>}/>
                  <Route path='/archiveprof/:id' element={<Archive/>}/>
                  <Route path='/classe/:name' element={<Classe/>}/>
                  <Route path='/classe2nd_A' element={<Classe2A/>}/>
                  <Route path='/addeleve' element={<AddEleve/>}/>
                  <Route path='/editeleve/:id' element={<EditEleve/>}/>
                  <Route path='/bulletin/:id' element={<Bulltin/>}/>
                  <Route path='/bulletin/1' element={<Bulltin/>}/>
            </Routes>
       </BrowserRouter>
    </div>
  );
}

export default App;
